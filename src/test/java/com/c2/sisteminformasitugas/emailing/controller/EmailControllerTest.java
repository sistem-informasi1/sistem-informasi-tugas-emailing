package com.c2.sisteminformasitugas.emailing.controller;

import com.c2.sisteminformasitugas.emailing.model.Email;
import com.c2.sisteminformasitugas.emailing.service.EmailSenderServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
public class EmailControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private EmailSenderServiceImpl emailSenderService;


    private Email email;


    @BeforeEach
    public void setUp() {
        String[] emailArr = {"dummy@dummy.com"};
        email = new Email(emailArr, "test", "test");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }


    @Test
    void testCreateEmail() throws Exception {
        doNothing().when(emailSenderService).sendSimpleEmail(email.toEmail, email.body, email.subject);
        mvc.perform(post("/email")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(email)))
                .andExpect(jsonPath("$.body").value("test"));

    }
}
