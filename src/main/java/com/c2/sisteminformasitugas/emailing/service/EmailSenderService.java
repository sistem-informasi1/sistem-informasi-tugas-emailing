package com.c2.sisteminformasitugas.emailing.service;

public interface EmailSenderService {

    void sendSimpleEmail(String[] toEmail,
                                String body,
                                String subject);
}
