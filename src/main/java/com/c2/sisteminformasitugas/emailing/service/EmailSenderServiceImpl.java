package com.c2.sisteminformasitugas.emailing.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailSenderServiceImpl implements EmailSenderService{

    @Autowired
    private JavaMailSender mailSender;

    public void sendSimpleEmail(String[] toEmail,
                                String body,
                                String subject) {
        var message = new SimpleMailMessage();

        for (String i : toEmail) {
            message.setFrom("email.situgas@gmail.com");
            message.setTo(i);
            message.setText(body);
            message.setSubject(subject);
            mailSender.send(message);
            System.out.println("Mail Send...");
        }

    }
}